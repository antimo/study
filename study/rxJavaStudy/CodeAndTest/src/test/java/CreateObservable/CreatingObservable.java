package CreateObservable;

import io.reactivex.Observable;
import org.junit.Test;

import java.util.ArrayList;

public class CreatingObservable {

    @Test
    public void createFromJust(){
        Observable<String> source =
                Observable.just("Alpha");

        source.subscribe(s -> System.out.println("RECEIVED: " + s));

    }

    public void createJust(){

        ArrayList<String> strings = new ArrayList<>();
        strings.add("one");
        strings.add("two");

        Observable<String> source =
                Observable.just("Alpha", "Beta", "Gamma", "Delta",
                        "Epsilon");

        source.subscribe(s -> System.out.println("RECEIVED: " + s));

    }


}
