package HotAndColdObservable;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import org.junit.Test;

public class ExampleonHotAndColdObservable {


    @Test
    public void coldObservable() {
        // This is a cold Observable
        Observable<String> source = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon");

        Consumer<Throwable> onError = Throwable::printStackTrace;

        source.map(String::length).filter(i -> i >= 5).subscribe(i ->  System.out.println("RECEIVED: " + i) , Throwable::printStackTrace);

        System.out.println("-----> Done first osservable");

        source.map(String::length).filter(i -> i >= 5).subscribe(i ->  System.out.println("RECEIVED: " + i) , Throwable::printStackTrace);

    }


    // Every time the Observable is subscirbed it re-emit the data (in this case same data,
    // for a SQLRx Observer data could change if DB is manipulated, but same query is executed)
    @Test
    public void coldObservableRemainTheSameIfFirstObserverExtractOtherData() {
        // This is a cold Observable
        Observable<String> source = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon");

        Consumer<Throwable> onError = Throwable::printStackTrace;

        source.subscribe(i ->  System.out.println("RECEIVED: " + i) , Throwable::printStackTrace);

        System.out.println("-----> Done first osservable");

        source.map(String::length).filter(i -> i >= 6).subscribe(i ->  System.out.println("RECEIVED: " + i) , Throwable::printStackTrace);

    }


}
