package ConsumeAndFilterObservable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CreateConsumeAndFilterObservable {


    //Canarian but COMPLExt test :)
    @Test
    public void givenObservable_whenZip_shouldAssertBlockingInASameThread() {
        // given
        List<String> letters = Arrays.asList("A", "B", "C", "D", "E");
        List<String> results = new ArrayList<>();
        Observable<String> observable = Observable.fromIterable(letters)
                .zipWith(Observable.range(1, Integer.MAX_VALUE), (string, index) -> index + "-" + string);

        // when
        observable.subscribe(results::add);

        // then
        assertThat(results, notNullValue());
        assertThat(results, hasSize(5));
        assertThat(results, hasItems("1-A", "2-B", "3-C", "4-D", "5-E"));
    }

    @Test
    public void simpleFilter() {
        List<String> items = Arrays.asList("one", "twoooo", "a");

        Observable<String> source = Observable.fromIterable(items);

        List<Integer> results = new ArrayList<>();
        source.map(String::length).filter(i -> i > 2)
                .subscribe(results::add);


        assertThat(results, notNullValue());
        assertThat(results, hasSize(2));
        assertThat(results, hasItems(3, 6));

    }

    @Test
    public void AnotherSimplerFilter() {
        List<String> items = Arrays.asList("auto", "airplane", "scooter");

        Observable<String> source = Observable.fromIterable(items);

        List<Integer> results = new ArrayList();

        source.map(String::length).filter(i -> i > 4).subscribe(results::add);


        assertThat(results, notNullValue());
        assertThat(results, hasSize(2));
        assertThat(results, hasItems(7));
    }


    @Test
    public void consumeWithHocObserver() {
        Observable<String> source = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon");
        Observer<Integer> myObserver = new Observer<Integer>() {
            @Override
            public void onSubscribe(Disposable d) {
                System.out.println("On subscribe");
                //do nothing with Disposable, disregard for now
            }

            @Override
            public void onNext(Integer value) {
                System.out.println("RECEIVED: " + value);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                System.out.println("Done!");
            }
        };
        source.map(String::length).filter(i -> i >= 5).subscribe(myObserver);
    }

    @Test
    public void consumeWithLambda() {
        Observable<String> source = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon");

        Consumer<Integer> onNext = i ->  System.out.println("RECEIVED: "          + i);

        Action onComplete = () -> System.out.println("Done!");

        Consumer<Throwable> onError = Throwable::printStackTrace;

        source.map(String::length).filter(i -> i >= 5).subscribe(onNext , onError, onComplete);
    }

    @Test
    public void anotherConsumeWithLambda() {
        Observable<String> source = Observable.just("Alpha", "Beta", "Gamma", "Delta", "Epsilon");

        Consumer<Throwable> onError = Throwable::printStackTrace;

        source.map(String::length).filter(i -> i >= 5).subscribe(i ->  System.out.println("RECEIVED: " + i) , Throwable::printStackTrace);
    }




}

