package functional.myexcersise;


import functional.Car;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

@FunctionalInterface
interface TestFunctionalInterface {
    boolean test(Car c);
}



public class CreateLambda {

    private static final Predicate<Car> RED_CAR_CRITERION
            = c -> c.getColor().equals("Red");

    private static final Predicate<LocalDate> isDateBefore
            = localDate -> localDate.isBefore(LocalDate.now() );

    public static void main(String[] args) {
        List<Car> cars = Arrays.asList(
                Car.withGasColorPassengers(6, "Red", "Fred", "Jim", "Sheila"),
                Car.withGasColorPassengers(3, "Octarine", "Rincewind", "Ridcully"),
                Car.withGasColorPassengers(9, "Black", "Weatherwax", "Magrat"),
                Car.withGasColorPassengers(7, "Green", "Valentine", "Gillian", "Anne", "Dr. Mahmoud"),
                Car.withGasColorPassengers(6, "Red", "Ender", "Hyrum", "Locke", "Bonzo")
        );

        TestFunctionalInterface testIfcolorIsRED = (c -> c.getColor().equals("RED"));

        Car aCar = Car.withGasColorPassengers(0, "RED");
        boolean isCarRed = testIfcolorIsRED.test(aCar);

        getListByPredicate(cars, RED_CAR_CRITERION);

        getListByPredicate(cars , hasAtLeast3Passenger);

        Iterable<LocalDate> localDates = Arrays.asList(LocalDate.now(), LocalDate.now().plusDays(1), LocalDate.now().plusDays(2) );
        getListByPredicate(localDates , dateAfterNow);

        Iterable<String> strings = Arrays.asList("Pippo" , "PIPPO");

        getListByPredicate(strings , allUpperCaseString);


    }

    private static <X> void getListByPredicate(Iterable<X> cars , Predicate<X> predicate){
        for (X x : cars) {
            if (predicate.test(x)) System.out.println(x);
        }
    };

    public static Predicate<LocalDate> dateAfterNow = localDate -> localDate.isAfter(LocalDate.now());

    public static Predicate<LocalDate> dateAfterParameterDate(LocalDate compareDate){
        return (localDate) -> localDate.isAfter(compareDate);
    };
//    = localDate -> localDate.isAfter(LocalDate.now());




    private static Predicate<Car> GREEN_CAR = c -> c.getColor().equals("Green");

    private static Predicate<Car> Black = c -> c.getColor().equals("Black");

    private static Predicate<Car> hasAtLeast3Passenger = c -> c.getPassengers().size() > 2;

    private static Predicate<String> allUpperCaseString = string -> string.toUpperCase().equals(string);

}
