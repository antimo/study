package fpij;

/**
 * Created by antimo on 04/12/2016.
 */
public class MyIterateString {

    public static void main(String[] args) {
        System.out.println("//" + "START:ITERATE_OUTPUT");

        final String str = "w00t";

        str.chars().forEach(System.out::println);
        System.out.println("//" + "END:ITERATE_OUTPUT");

        System.out.println("//" + "WITH OUTPUT CONVERSION");
        str.chars().
                mapToObj(ch -> Character.valueOf((char)ch)).
                forEach(System.out::println);
        System.out.println("// END WITH CONVERSION");
    }
}
