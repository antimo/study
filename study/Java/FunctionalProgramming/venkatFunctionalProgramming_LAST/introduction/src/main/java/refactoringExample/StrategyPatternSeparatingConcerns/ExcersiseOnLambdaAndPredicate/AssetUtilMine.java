package refactoringExample.StrategyPatternSeparatingConcerns.ExcersiseOnLambdaAndPredicate;

import refactoringExample.StrategyPatternSeparatingConcerns.FirstImplementation.Asset;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class AssetUtilMine {


    public static void main(final String[] args) {
        final List<Asset> assets = Arrays.asList(
                new Asset(Asset.AssetType.BOND, 1000),
                new Asset(Asset.AssetType.BOND, 2000),
                new Asset(Asset.AssetType.STOCK, 3000),
                new Asset(Asset.AssetType.STOCK, 4000)
        );

        Predicate<Asset> assetIsBOND= asset -> asset.getType() == Asset.AssetType.BOND;

        Predicate<Asset> allAssett= asset -> true;


        System.out.println("//" + "START:TOTAL_ALL_OUTPUT");
        System.out.println("Total of all assets: " + mytotalAssetValuesForTypeOfAsset(assets , allAssett));
        System.out.println("//" + "END:TOTAL_ALL_OUTPUT");

        System.out.println("//" + "START:TOTAL_ALL_OUTPUT");
        System.out.println("Total of Bond assets: " + mytotalAssetValuesForTypeOfAsset(assets, assetIsBOND));
        System.out.println("//" + "END:TOTAL_ALL_OUTPUT");

    }

    private static int mytotalAssetValues(List<Asset> assets) {
        int sum = assets.stream().mapToInt(asset -> asset.getValue()).sum();
        return sum;
    }

    private static int mytotalAssetValuesForTypeOfAsset(List<Asset> assets , Predicate<Asset> assetType) {
        int sum = assets.stream().filter(assetType).mapToInt(asset -> asset.getValue()).sum();
        return sum;
    }



}
