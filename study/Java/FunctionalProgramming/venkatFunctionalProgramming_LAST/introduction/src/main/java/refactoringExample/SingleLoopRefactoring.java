package refactoringExample;

import java.util.ArrayList;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SingleLoopRefactoring {

    public List<Integer> simpleLoop(List<Integer>integerList){
        List<Integer> result = new ArrayList<>();

        for (int i = 1; i <= 10; i++) {
            if (10 % i == 0) {
                result.add(i);
            }
        }
        return result;
    }

    //    final Predicate<String> lengthMoreOf10 = name -> name.length() > 10;
    public final Predicate<Integer> divide10 = integer -> 10 % integer.intValue() == 0;
    public final IntPredicate divide10int = intValue -> 10 % intValue == 0;



    public List<Integer> simpleLoopFirstRefactoring(){
        List<Integer> result = new ArrayList<>();

        for (int i = 1; i <= 10; i++) {
            if (divide10int.test(i)) {
                result.add(i);
            }
        }
        return result;
    }


    public List<Integer> usingStream() {
        IntPredicate intPredicate = i -> 10 % i == 0;
//        List<Integer> result1 = IntStream.rangeClosed(1, 10).filter(intPredicate).boxed().collect(Collectors.toList());

        List<Integer> result2 = IntStream.rangeClosed(1,10).filter(divide10int).peek(System.out::println).boxed().collect(Collectors.toList());

        return result2;
    }



    // https://stackoverflow.com/questions/31629324/how-to-perform-nested-if-statements-using-java-8-lambda?answertab=active#tab-top
    public List<Integer> nonIsomorphicaTransformation(List<Integer>integerList){
        List<Integer> result = new ArrayList<>();

        for (int i = 1; i <= 10; i++) {
            if (10 % i == 0) {
                result.add(i);
                if (i != 5) {
                    result.add(10 / i);
                }
            }
        }
        return result;
    }

}
