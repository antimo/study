package fpij;

import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

public class FindElementInStream {

    List<String> friends = fpij.Folks.friends;

    @Test
    public void findElementInStream(){
        List<String> friendsStartingWithN = friends.stream().filter(name -> name.startsWith("N")).collect(Collectors.toList());

        friendsStartingWithN.forEach(System.out::println);
//        Same result
//        friendsStartingWithN.stream().forEach(System.out::println);

    }

}

