package refactoringExample.loop;

import org.junit.Before;
import org.junit.Test;
import refactoringExample.SingleLoopRefactoring;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SingleLoopRefactoringTest {

    private SingleLoopRefactoring singleLoopRefactoringUnderTest;

    @Before
    public void setUp() {
        singleLoopRefactoringUnderTest = new SingleLoopRefactoring();
    }

    @Test
    public void testSimpleLoop() {
        // Setup
        final List<Integer> integerList = Arrays.asList();
        Integer[] arrayExpectedResult = new Integer[]{1,2,5,10};
        final List<Integer> expectedResult = Arrays.asList(arrayExpectedResult);

        // Run the test
        final List<Integer> result = singleLoopRefactoringUnderTest.simpleLoop(integerList);

        // Verify the results
        assertEquals(expectedResult, result);
    }


    @Test
    public void testSimpleLoopFirstRefactoring() {
        // Setup
        final List<Integer> integerList = Arrays.asList();
        Integer[] arrayExpectedResult = new Integer[]{1,2,5,10};
        final List<Integer> expectedResult = Arrays.asList(arrayExpectedResult);

        // Run the test
        final List<Integer> result = singleLoopRefactoringUnderTest.simpleLoopFirstRefactoring();

        // Verify the results
        assertEquals(expectedResult, result);
    }


    @Test
    public void testSimpleLoopWithStream() {
        // Setup
        final List<Integer> integerList = Arrays.asList();
        Integer[] arrayExpectedResult = new Integer[]{1,2,5,10};
        final List<Integer> expectedResult = Arrays.asList(arrayExpectedResult);

        // Run the test
        final List<Integer> result = singleLoopRefactoringUnderTest.usingStream();

        // Verify the results
        assertEquals(expectedResult, result);
    }



    @Test
    public void testIntegerListFilter() {
        // Setup
        final List<Integer> integerList = Arrays.asList();
        final List<Integer> expectedResult = Arrays.asList();

        // Run the test
        final List<Integer> result = singleLoopRefactoringUnderTest.nonIsomorphicaTransformation(integerList);

        // Verify the results
        assertEquals(expectedResult, result);
    }
}
