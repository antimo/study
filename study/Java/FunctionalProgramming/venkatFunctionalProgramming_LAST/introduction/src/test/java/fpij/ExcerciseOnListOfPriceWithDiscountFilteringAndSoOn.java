package fpij;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

public class ExcerciseOnListOfPriceWithDiscountFilteringAndSoOn {

    private int numberOfPRices = 8;
    private BigDecimal sumOfPrices = new BigDecimal(167);



    @org.junit.Before
    public void setUp() throws Exception {
    }

    @Test
    public void canaryTest() throws Exception {
        assertTrue(true);

    }

    @Test
    public void readAllPriceImperativeMode() throws Exception {
        List<BigDecimal> myPrices = Prices.prices;
        int i = 0;
        for (BigDecimal price:myPrices) {
            System.out.println("Price " + price);
            i++;
        }
        assertTrue( i == numberOfPRices);
    }

    @Test
    public void getTotalPriceFunctional() throws Exception {
        BigDecimal result = Prices.prices.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println(result);
        assertTrue(result.compareTo(sumOfPrices) == 0);
    }

    @Test
    public void getPriceOver20() throws Exception {
         List<BigDecimal> pricesOver20 = Prices.
                prices.stream().
                filter(pri -> pri.compareTo(BigDecimal.valueOf(20)) > 0)
                .collect(Collectors.toCollection(ArrayList::new));
         assertTrue(pricesOver20.size() == 2);
    }

    @Test
    public void getPricesOver20DiscountOf10() throws Exception {
        List<BigDecimal> pricesOver20DiscountedBy10 = Prices.prices.stream()
                .filter(price -> price.compareTo(BigDecimal.valueOf(20)) > 0)
                .map(price -> price.multiply(BigDecimal.valueOf(.9)))
                .collect(Collectors.toCollection(ArrayList::new));

        assertTrue(pricesOver20DiscountedBy10.size() == 2);

    }
}
