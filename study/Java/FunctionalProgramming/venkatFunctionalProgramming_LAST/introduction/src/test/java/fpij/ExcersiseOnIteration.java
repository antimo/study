package fpij;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.function.Consumer;

public class ExcersiseOnIteration {

    @Test
    public void imperativeIteraction() {

        for (int i = 0; i <Prices.prices.size() ; i++) {
            System.out.println(Prices.prices.get(i));
        }

        // Another Imperative
        for (BigDecimal price:Prices.prices){
            System.out.println(price);
        }

    }

    @Test
    public void functionalIterationWithConsumer(){
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>START WITH EXPLICIT CONSUMER<<<<<<<<<<<<<<<<<<<<<<<<");
        Prices.prices.forEach(new Consumer<BigDecimal>() {
            @Override
            public void accept(BigDecimal bigDecimal) {
                System.out.println(bigDecimal);
            }
        });

        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>END WITH EXPLICIT CONSUMER<<<<<<<<<<<<<<<<<<<<<<<<");


        Prices.prices.forEach((final BigDecimal bigDecimal) -> System.out.println(bigDecimal));

        Prices.prices.forEach((bigDecimal) -> System.out.println(bigDecimal));

        Prices.prices.forEach(System.out::println);

    }

}
