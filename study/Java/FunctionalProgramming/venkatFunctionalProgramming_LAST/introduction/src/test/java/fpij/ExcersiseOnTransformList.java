package fpij;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExcersiseOnTransformList {

    List<String> upperCaseNames = new ArrayList<>();
    List<String> friends = fpij.Folks.friends;

    @Test
    public void imperativeTransformer() {
        List<String> friends = fpij.Folks.friends;

        for (String name : friends) {
            upperCaseNames.add(name.toUpperCase());
        }

        friends.forEach(name -> upperCaseNames.add(name));

    }

    @Test
    public void functionalTransformer() {
        friends.stream().map(name -> name.toUpperCase()).forEach(name -> upperCaseNames.add(name));

        // Print result
        upperCaseNames.stream().forEach(System.out::println);

        // Or if we use Array instead of list
        System.out.println(Arrays.toString(upperCaseNames.toArray()));
    }
}
