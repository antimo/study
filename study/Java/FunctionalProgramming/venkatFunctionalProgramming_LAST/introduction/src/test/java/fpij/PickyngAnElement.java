import fpij.Folks;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

public class PickyngAnElement {
    public static void pickName(
            final List<String> names, final String startingLetter) {
        String foundName = null;
        for(String name : names) {
            if(name.startsWith(startingLetter)) {
                foundName = name;
                break;
            }
        }
        System.out.print(String.format("A name starting with %s: ", startingLetter));

        if(foundName != null) {
            System.out.println(foundName);
        } else {
            System.out.println("No name found");
        }
    }

    @Test
    public void pickingImperative() {
        pickName(Folks.friends , "B");
    }

    @Test
    public void pickyngWithStream() {
        Optional<String> b = Folks.friends.stream().filter(name -> name.startsWith("B")).findFirst();

        System.out.println("Found name " + b.orElse("No name found"));
    }
}
