package fpij;

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;
import java.util.function.BinaryOperator;

import static org.junit.Assert.*;

public class ReducingCollections {

    @Test
    public void totalNumOfCharacter() {
        fpij.Folks.friends.stream().mapToInt(name -> name.length()).sum();
    }

    @Test
    public void findTheShorterOne(){
        Optional<String> reduce = fpij.Folks.friends.stream().reduce(getShorterString());

        System.out.println("Result " + reduce.orElse("No RESULT FRON REDUCE"));
        assertTrue(reduce.get().equals("one"));
    }

    @Test
    public void useDefaultValueForReduce() {
        String nam = "Na";
        String default_name = fpij.Folks.friends.stream().reduce(nam, getShorterString());

        System.out.println("REsult " + default_name);
        assertTrue(default_name.equals(nam));
    }

    private BinaryOperator<String> getShorterString() {
        return (name1, name2) -> name2.length() > name1.length() ? name1 : name2;
    }
}
