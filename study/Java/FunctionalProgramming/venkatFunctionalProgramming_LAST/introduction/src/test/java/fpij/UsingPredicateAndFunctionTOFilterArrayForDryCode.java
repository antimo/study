package fpij;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class UsingPredicateAndFunctionTOFilterArrayForDryCode {

    List<String> friends = fpij.Folks.friends;
    List<String> other = fpij.Folks.comrades;

    final Predicate<String> startsWithN = name -> name.startsWith("N");
    final Predicate<String> startsWithB = name -> name.startsWith("B");

    final Predicate<String> lengthMoreOf10 = name -> name.length() > 10;


    // Use a fixed predicate
    @Test
    public void usingAPredicate(){

        friends.stream().filter(startsWithN).forEach(System.out::println);

        other.stream().filter(startsWithN).forEach(System.out::println);

    }

    @Test
    public void usingTwoPredicate(){

        friends.stream().filter(startsWithN).forEach(System.out::println);
        System.out.println("Start with B");
        friends.stream().filter(startsWithB).forEach(System.out::println);

    }

    // A Predicate<T> takes in one parameter of type T and returns a boolean result
    // Create a parametric predicate
    public static Predicate<String> checkIfStartsWith(final String letter){
        return name -> name.startsWith(letter);
    }


    // Create a static parametic Predicate
    @Test
    public void parametricStaticDefinedPredicate(){
        friends.stream().filter(checkIfStartsWith("N")).forEach(System.out::println);
        final String charcterToSearch = "B";
        System.out.println("Start with " + charcterToSearch);
        friends.stream().filter(checkIfStartsWith(charcterToSearch)).forEach(System.out::println);
    }

    // Predicate on String length
    public static Predicate<String> checkIfLenMoreOf(final int lenToCheck){
        return name -> name.length() > lenToCheck;
    }

    @Test
    public void parametricStaticPredicateOnLength(){
        friends.stream().filter(checkIfLenMoreOf(5)).forEach(System.out::println);
    }

    // A Function<T, R> represents a function that takes a parameter of type T and returns a result of type R.
    // Here we define a Function that will return a Predicate<String> i.e
    // letter is the parameter of Type T (Integer) so input value is an INTEGER in this case!!
    final Function<Integer , Predicate<String>> checkIfLenMoreOfNumber = (Integer number) -> {
        Predicate<String> checkLen = (String name) -> name.length()>number;
        return checkLen;
    };



    final Function<String, Predicate<String>> startWithLetterFirstVersion = (String letter) -> {
        Predicate<String> checkStarts = (String name) -> name.startsWith(letter);
        return checkStarts;
    };


    @Test
    public void parametricFunctionDefinedPredicate(){
        friends.stream().filter(startWithLetterFirstVersion.apply("N")).forEach(System.out::println);
        final String charcterToSearch = "B";
        System.out.println("Start with " + charcterToSearch);
        friends.stream().filter(checkIfStartsWith(charcterToSearch)).forEach(System.out::println);
    }

    @Test
    public void parametricFunctionDefinedPredicateOnLengthInteger(){
        friends.stream().filter(checkIfLenMoreOfNumber.apply(3)).forEach(System.out::println);
    }

    // Eliminate Boilerplate
//    final Function<Integer , Predicate<String>> checkIfLenMoreOfNumber = (Integer number) -> {
//        Predicate<String> checkLen = (String name) -> name.length()>number;
//        return checkLen;
//    };
    final Function<Integer , Predicate<String>> checkIfLenMoreOfNumberWithoutBoilerplate =
        number -> name -> name.length() > number;

    // Another Predicate for studyng...
    // Check if name have length less than
//    final Predicate<String> lengthMoreOf10 = name -> name.length() > 10;

    final Predicate<String> checkLenLEssThan = name -> name.length() < 10;

    Function<Integer, Predicate<String>>  checkIFLenLessThan = (Integer number) ->{
        Predicate<String> predicate =  name -> name.length() < number;
        return predicate;
    };

    // Without Boilerplate
    Function<Integer, Predicate<String>>  checkIFLenLessThanWhitoutBoilerplate =
         number ->  name -> name.length() < number;


    // Another test for Predicate
    Predicate<Integer> isMoreThan5 = y -> y > 5;

    // Define a Function to make  a parametric call to Predicate
    final Function<Integer ,Predicate<Integer>> isMoreThanXFunction = (Integer input) ->{
        Predicate<Integer> predicate = value -> value > input;
        return predicate;
    };

    // Function without Boilerplare
    final Function<Integer , Predicate<Integer>> isMoreThanXWithoutClutterCode =
            functionInput -> predicateInput -> predicateInput > functionInput;

    @Test
    public void checkIfIsMoreThan(){

        Integer a = 5;
        boolean test = isMoreThan5.test(6);
        Assert.assertTrue(test == true);
        boolean test2 = isMoreThan5.test(4);
        Assert.assertTrue(test2 == false);

    }

    @Test
    public void checkIfIsMoreThanXWithFunction(){
//        Integer a = 5;
        boolean test = isMoreThanXFunction.apply(15).test(16);
        Assert.assertTrue(test == true);

    }

    @Test
    public void checkIfIsMoreThanXWithFunctionWithoutClutter(){
//        Integer a = 5;
        boolean test = isMoreThanXWithoutClutterCode.apply(15).test(16);
        Assert.assertTrue(test == true);

    }



}
