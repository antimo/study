package com.example.jpa;

import com.example.jpa.model.Comment;
import com.example.jpa.model.Post;
import com.example.jpa.model.PostType;
import com.example.jpa.repository.CommentRepository;
import com.example.jpa.repository.PostRepository;
import com.example.jpa.repository.PostTypeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RepositoryTest {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    PostTypeRepository postTypeRepository;

    @Autowired
    private TestEntityManager testEntityManager;


    private String post_title = "Post title";;



    @Test
    public void testFindByName() {
        String post_title = "Post title";
        Post post= postRepository.findByTitle(post_title);

        assertThat(post != null);
    }

    @Test
    public void findByTitle_ReturnsExactPost() throws Exception {
        PostType postType = new PostType();
        postType.setDescription("First Type");
        Post post = testEntityManager.persistFlushFind(new Post(post_title, "Descrizione" , "Content", postType));
        Post findedPost = this.postRepository.findByTitle(post_title);
        assertThat(post.getTitle()).isEqualTo(post.getTitle());
        assertThat(post.getDescription()).isEqualTo(findedPost.getDescription());
    }


    @Test
    public void testOneToManySave() throws Exception {
        PostType postType = new PostType();
        postType.setDescription("First Type");
        Post post = new Post(post_title, "Descrizione" , "Content" , postType);

        String commentOneText = "Great Post!";

        Comment comment1 = new Comment(commentOneText);
        comment1.setPost(post);

        String comment2Text = "Really helpful Post. Thanks a lot!";
        Comment comment2 = new Comment(comment2Text);
        comment2.setPost(post);

        post.getComments().add(comment1);
        post.getComments().add(comment2);

        // Test Saved Post
        Post savedPost = testEntityManager.persistFlushFind(post);
        Post findedPost = this.postRepository.findByTitle(post_title);
        assertThat(post.getTitle()).isEqualTo(post.getTitle());
        assertThat(post.getDescription()).isEqualTo(findedPost.getDescription());


//        Optional findById1 = commentRepository.findById(1L);
//        Optional findById2 = commentRepository.findById(2L);
//
//        Comment comment1Find = (Comment) findById1.get();
//        Comment comment2Find = (Comment) findById2.get();
//
//        assertThat(comment1Find.getText()).isEqualTo(commentOneText);
//        assertThat(comment2Find.getText()).isEqualTo(comment2Text);

        List<Comment> byText1 = commentRepository.findByText(commentOneText);
        assertThat(byText1.get(0).getText()).isEqualTo(commentOneText);
        System.out.println("----->>>>>Comment1 Text" + byText1.get(0).getText());

        List<Comment> byText2= commentRepository.findByText(comment2Text);
        assertThat(byText2.get(0).getText()).isEqualTo(comment2Text);
        System.out.println("----->>>>>Comment2 Text" + byText2.get(0).getText());



    }

    @Test
    public void testOneToManyLoadThenSaveChangingValue() throws Exception {
        // Create a Post with comment and PostType
        // PostType --> Post :  ManyToOne

        PostType postType = new PostType();
        postType.setDescription("First Type");

        postTypeRepository.save(postType);

        Post post = new Post(post_title, "Descrizione" , "Content" , postType);

        String commentOneText = "Great Post!";

        Comment comment1 = new Comment(commentOneText);
        comment1.setPost(post);

        post.getComments().add(comment1);

        // Test Saved Post
        Post findedPost = this.postRepository.findByTitle(post_title);


        // Create a new PostType and save
        PostType postTypeNew = new PostType();
        String anotherPostTypeDescription = "Another Type 2";
        postTypeNew.setDescription(anotherPostTypeDescription);
        PostType postType1 = testEntityManager.persistFlushFind(postTypeNew);

        // Change type to PostType for our FindedPost
        findedPost.setPostType(postTypeNew);

        // Save Post with changed PostType
        Post post1 = testEntityManager.persistFlushFind(findedPost);

//        List<Post> postList = (List<Post>) postTypeNew.getPostList();
//        postList.remove(post1);

        findedPost = this.postRepository.findByTitle(post_title);
        assertThat(findedPost.getPostType().getDescription()).isEqualTo(anotherPostTypeDescription);

        // Create a new PostType and save
        PostType postTypeNewAnother = new PostType();
        String thirdPostTypeDescription = "Another Type 3";
        postTypeNewAnother.setDescription(thirdPostTypeDescription);
        PostType postType2 = testEntityManager.persistFlushFind(postTypeNewAnother);

        findedPost.setPostType(postType2);

        Post post3 = testEntityManager.persistFlushFind(findedPost);

//        List<Post> postList = (List<Post>) postTypeNew.getPostList();
//        postList.remove(post1);

        findedPost = this.postRepository.findByTitle(post_title);
        assertThat(post3.getPostType().getDescription()).isEqualTo(thirdPostTypeDescription);


    }


}