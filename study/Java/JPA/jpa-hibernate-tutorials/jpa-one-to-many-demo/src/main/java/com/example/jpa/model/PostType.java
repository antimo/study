package com.example.jpa.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "postType")
public class PostType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_PostType;

    @NotNull
    @Size(max = 250)
    private String description;

    public List<Post> getPostList() {
        return postList;
    }

    public void setPostList(List<Post> postList) {
        this.postList = postList;
    }

    @OneToMany(mappedBy = "postType")
    List<Post> postList;

    public Long getId_PostType() {
        return id_PostType;
    }

    public void setId_PostType(Long id_PostType) {
        this.id_PostType = id_PostType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
