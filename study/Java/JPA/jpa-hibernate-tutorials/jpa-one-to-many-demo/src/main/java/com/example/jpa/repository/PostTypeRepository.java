package com.example.jpa.repository;

import com.example.jpa.model.PostType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PostTypeRepository extends JpaRepository<PostType, Long> {

//    public List<Comment> findByText(String text);

}
