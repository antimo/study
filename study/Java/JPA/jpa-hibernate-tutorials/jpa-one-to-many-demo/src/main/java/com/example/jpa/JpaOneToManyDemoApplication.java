package com.example.jpa;

import com.example.jpa.repository.CommentRepository;
import com.example.jpa.repository.PostRepository;
import com.example.jpa.repository.PostTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaOneToManyDemoApplication implements CommandLineRunner {

	@Autowired
	private PostRepository postRepository;

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private PostTypeRepository postTypeRepository;

	public static void main(String[] args) {
		SpringApplication.run(JpaOneToManyDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		// Cleanup Database tables
//		commentRepository.deleteAllInBatch();
//		postRepository.deleteAllInBatch();
//
//		// ======================================
//
//		PostType postType = new PostType();
//		postType.setDescription("First Type");
//
////		postTypeRepository.save(postType);
//
//
//		Post post = new Post("Hibernate One-To-Many Mapping Example",
//				"Learn how to use one to many mapping in hibernate",
//				"Entire Post Content with sample code" ,postType);
//
//		String commentText = "Great Post!";
//		Comment comment1 = new Comment(commentText);
//		comment1.setPost(post);
//
//		String anotherCommentText = "Really helpful Post. Thanks a lot!";
//		Comment comment2 = new Comment(anotherCommentText);
//		comment2.setPost(post);
//
//		post.getComments().add(comment1);
//		post.getComments().add(comment2);
//
//		postRepository.save(post);
//
//		List<Comment> byText = commentRepository.findByText(commentText);
//		System.out.println(byText.get(0).getText());
//
//		List<Comment> byText2= commentRepository.findByText(anotherCommentText);
//		System.out.println(byText2.get(0).getText());

		// ======================================

	}
}
