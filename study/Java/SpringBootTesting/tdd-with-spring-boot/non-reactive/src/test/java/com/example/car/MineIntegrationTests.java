package com.example.car;

import antimo.myexample.domain.MyCar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MineIntegrationTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void testEndpoint() {
        //arrange

        //act
        ResponseEntity<MyCar> response = testRestTemplate.getForEntity("/cars/prius", MyCar.class);

        //assert
        assertThat(response.getStatusCode().is2xxSuccessful());
        assertThat(response.getBody().getName()).isEqualTo("prius");
        assertThat(response.getBody().getType()).isEqualTo("hibryd");

    }
}
