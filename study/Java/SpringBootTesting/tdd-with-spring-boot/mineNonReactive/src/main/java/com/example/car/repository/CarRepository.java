package com.example.car.repository;

import com.example.car.domain.Car;
import org.springframework.data.repository.CrudRepository;

public interface CarRepository extends CrudRepository<Car, Long> {
    public Car findByName(String name);
}
