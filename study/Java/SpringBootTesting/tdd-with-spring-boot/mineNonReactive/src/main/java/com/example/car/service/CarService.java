package com.example.car.service;

import com.example.car.CarNotFoundException;
import com.example.car.domain.Car;
import com.example.car.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarService {

    @Autowired
    CarRepository carRepository;

    public Car getCarDetail(String name) {
        Car carByName = carRepository.findByName(name);
        if (carByName == null){
            throw new CarNotFoundException();
        }
        return carByName;
    }
}
