package com.example.car.repository;

import com.example.car.domain.Car;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CarRepositoryTest {

    @Autowired
    private CarRepository repository;

    @Autowired
    private TestEntityManager testEntityManager;

//    @Test
//    public void findByName() {
//        Car savedCar = entityManager.persistFlushFind(new Car("prius", "hybrid"));
//        Car car = this.repository.findByName("prius");
//        assertThat(car.getName()).isEqualTo(savedCar.getName());
//        assertThat(car.getType()).isEqualTo(savedCar.getType());
//
//    }


    @Test
    public void testFindByName() {
        Car prius = repository.findByName("prius");

        assertThat(prius != null);
    }

    @Test
    public void findByName_ReturnsCar() throws Exception {
        Car savedCar = testEntityManager.persistFlushFind(new Car("prius", "hybrid"));
        Car car = this.repository.findByName("prius");
        assertThat(car.getName()).isEqualTo(savedCar.getName());
        assertThat(car.getType()).isEqualTo(savedCar.getType());
    }


}