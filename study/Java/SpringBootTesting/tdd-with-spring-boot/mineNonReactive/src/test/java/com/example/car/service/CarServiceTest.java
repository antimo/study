package com.example.car.service;

import com.example.car.CarNotFoundException;
import com.example.car.domain.Car;
import com.example.car.repository.CarRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceTest {

    private CarService carServiceUnderTest;

    @Mock
    CarRepository carRepository;

    @Before
    public void setUp() {
        carServiceUnderTest = new CarService();
        carServiceUnderTest.carRepository = carRepository;
    }

    @Test
    public void testGetCarDetail() {
        // Setup
        final Car expectedResult = new Car("prius" , "hybrid");
        given(carRepository.findByName("prius")).willReturn(expectedResult);

        // Run the test
        final Car result = carServiceUnderTest.getCarDetail("prius");

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test(expected = CarNotFoundException.class)
    public void testGetCarDetail_NOTFOUND() {
        // Setup
        final Car expectedResult = new Car("prius" , "hybrid");
//        given(carRepository.findByName("prius")).willReturn(null);

        // Run the test
        final Car result = carServiceUnderTest.getCarDetail("prius");

        // Verify the results -> Nothing to do we have an Exception
    }

}
