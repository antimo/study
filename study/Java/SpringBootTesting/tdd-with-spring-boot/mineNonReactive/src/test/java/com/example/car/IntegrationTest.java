package com.example.car;

import com.example.car.domain.Car;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void testName() {
        //arrange

        //act
        ResponseEntity<Car> testRestTemplateForEntity = testRestTemplate.getForEntity("/cars/prius", Car.class);

        //assert
        assertThat(testRestTemplateForEntity.getStatusCode().is2xxSuccessful());
        assertThat(testRestTemplateForEntity.getBody().getName()).isEqualTo("prius");
        assertThat(testRestTemplateForEntity.getBody().getType()).isEqualTo("hibryd");

    }
}
