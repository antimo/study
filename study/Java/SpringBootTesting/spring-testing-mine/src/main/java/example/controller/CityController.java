package example.controller;

import example.city.City;
import example.city.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CityController {

    String HelloWorldMessageFormCity = "Hello world from ";
    String cityWithAPopulation = " with a population of ";

    @Autowired
    private CityRepository cityRepository;

    @GetMapping("city/hello")
    public String hello() {
        return "Hello World!";
    }

    @GetMapping("/city/{cityName}")
    public String anotherHello(@PathVariable String cityName){
        City city = cityRepository.findByName(cityName);

        return HelloWorldMessageFormCity + cityName + cityWithAPopulation + city.getPopulation();
    }

}
