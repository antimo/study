package example.integration.controller;

import example.city.City;
import example.city.CityRepository;
import example.controller.CityController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CityController.class)
public class CityControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CityRepository cityRepository;

    @Test
    public void shouldReturnHelloWorld() throws Exception {
        mockMvc.perform(get("/city/hello"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("Hello World!"));
    }

    @Test
    public void shouldReturnPopulation() throws Exception {
        City paris = new City("Paris", 400);
        given(cityRepository.findByName("Paris")).willReturn(paris);

        mockMvc.perform(get("/city/Paris"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("Hello world from city with a population of 400"));
    }


}
