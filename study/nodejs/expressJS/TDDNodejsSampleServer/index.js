const express = require('express')
const app = express()
const port = 3000

app.get('/IAD', (req, res) => res.json({
    "delay": "true",
    "IATA": "IAD",
    "state": "California",
    "name": "Washington Dallas Airport"
}))

app.get('/SFO', (req, res) => res.json({
    "delay": "true",
    "IATA": "SFO",
    "state": "California",
    "name": "San Francisco International"
}))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
