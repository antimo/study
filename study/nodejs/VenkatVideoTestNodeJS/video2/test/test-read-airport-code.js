let expect = require('chai').expect;
let readFile = require('../src/airport-status').readFile;
require('chai').use(require('chai-as-promised'));

describe('Canarian Test', function () {
    it('Canarian test', function () {
        expect(true).to.be.true;
    });

    it('should resolve reading a valid file', function () {
        return readFile('./test/test-read-airport-code.js');
    });

    it('should not resolve reading a non valid file', function () {

        expect(readFile('./test-read-airport-code.jos')).to.be.rejected;
    });

});

