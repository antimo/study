let expect = require('chai').expect;
require('chai').use(require('chai-as-promised'));
let processFile = require('../src/airport-status').processFile;

describe('Canary test', function () {
    it('Canary Test', function () {
        expect(true).to.be.eql(true);
    });
});

describe('Process file contents', function () {
    it('should return airport code for a valid code file', function () {
        var content = "IAH\nIAD\nSFO";

        var airportCodes = processFile(content);

        expect(airportCodes.length).to.be.eql(3);
        expect(airportCodes).to.be.eql(['IAH', 'IAD', 'SFO']);
    });

    it('It should return empty array if the content is empty ', function () {
        var content = "";

        var airportCodes = processFile(content);

        expect(airportCodes).to.be.eql([]);
    });

    it('It should skip one empty line', function () {
        var content = "IAH\n     \nSFO";

        var airportCodes = processFile(content);

        //expect(airportCodes.length).to.be.eql(2);
        expect(airportCodes).to.be.eql(['IAH', 'SFO']);

    });

    it('Should file if containit a space in ariport name', function () {
        var content = "IAH\nSF O";

        //expect(airportCodes.length).to.be.eql(2);
        expect(function(){processFile(content)}).to.throw(Error);

    });

    it('Should file if contain a space in airport name', function () {
        var content = "IAH\nSF O";

        //expect(airportCodes.length).to.be.eql(2);
        expect(function(){processFile(content)}).to.throw(Error);

    });

    it('Should fail if airport code is longe than', function () {
        var content = "IAH\nSF0O";

        //expect(airportCodes.length).to.be.eql(2);
        expect(function(){processFile(content)}).to.throw(Error);

    });

    it('Should fail if airport code is longe than', function () {
        var content = "IAH\nSF";

        //expect(airportCodes.length).to.be.eql(2);
        expect(function(){processFile(content)}).to.throw(Error);

    });


    it('should convert  the content of the file in a string', function () {
        var content = new Buffer("IAH\nIAD\nSFO");

        var airportCodes = processFile(content);

        expect(airportCodes.length).to.be.eql(3);
        expect(airportCodes).to.be.eql(['IAH', 'IAD', 'SFO']);

    });
});
