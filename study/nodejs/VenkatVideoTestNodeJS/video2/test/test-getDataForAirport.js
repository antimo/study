let expect = require('chai').expect;
let readFile = require('../src/airport-status').readFile;
let airportStatus = require('../src/airport-status');
require('chai').use(require('chai-as-promised'));

describe('Test get Data For Airport', function () {
    it('should return a promise', function () {
        airportStatus.getDataForAnAirport("IAD");
    });

    it('should resolve a Promise that return a JSON with airport code', function () {
        return expect(airportStatus.getDataForAnAirport("IAD"))
            .to.eventually.have.property('IATA');
    });

    it('should resolve a Promise that return a JSON with airport name', function () {
        return expect(airportStatus.getDataForAnAirport("IAD"))
            .to.eventually.have.property('IATA','IAD');
    });

    it('should resolve a Promise that return a JSON with airport name', function () {
        return expect(airportStatus.getDataForAnAirport("IAD"))
            .to.eventually.have.property('name','Washington Dallas Airport');
    });
    it('should resolve a Promise that return a JSON with another airport name', function () {
        return expect(airportStatus.getDataForAnAirport("SFO"))
            .to.eventually.have.property('name','San Francisco International');
    });


});
