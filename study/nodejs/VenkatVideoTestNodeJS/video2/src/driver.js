let airportStatus = require("../src/airport-status");

airportStatus.readFile('./airportcode.txt')
    .then(airportStatus.processFileContent)
    .then(content =>console.log(content.toString()) )
    .catch(err => console.log('Error ' + err));
