let fs = require('fs-promise');
let http = require ('http');

let airportStatus = {
    readFile: function (filename) {
        return fs.readFile(filename);
    },

    processFile: function (content) {
        let airportCodes = content
            .toString()
            .split('\n')
            .filter(value => value.trim() != "");

        /*    airportCodes.forEach(function (airportCode){
                if (airportCode.includes(" ") ||
                    airportCode.length != 3 ) throw new Error('Invalid Airport code');
            });*/

        if (airportCodes.filter(airportCode => airportCode.length !== 3 || airportCode.includes(" ")).length > 0)
            throw new Error('Invalid Airport code');

        return airportCodes;
    },
    getDataForAnAirport : function (airportCode){
        return new Promise(function(resolve,reject){
            let url = ` http://localhost:3000/${airportCode}` ;
            data = '';

            http.get(url , function(response){
                console.log(response.statusCode);
                response.on('data' , chunk => data += chunk);
                response.on('end' , () => console.log(data));
                resolve(JSON.parse(data));
            });
        })


        return Promise.resolve({'IATA' : 'IAD' , name : 'Washington Dallas Airport'});
    }
};

module.exports = airportStatus;
